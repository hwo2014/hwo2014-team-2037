require 'json'
require 'socket'
require 'pry-debugger'
require('awesome_print')

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    @bot_name = bot_name
    @positions = []
    @throttles = []
    @angles = []
    @speeds = []
    @car_weight = 32.0
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def play_tick(tcp, current_bot)
    current_bot['piecePosition']['totalDistance'] = distance(current_bot['piecePosition'])

    @positions.push(@current_position = current_bot['piecePosition'])
    @speeds.push(@current_speed = speed(4))
    @current_lane = current_bot['piecePosition']['laneIndex']
    @angles.push(@current_angle = current_bot['angle'])
    tmsg = throttle_message(throttle = naive_throttle)
    @throttles.push throttle
    ap ['  index |  speed | throttle | angle', 1.upto(25).map {|n| sprintf "%2d | %3.3f | %3.3f | %3.3f", n, @speeds[-n].to_f**2, @throttles[-n].to_f, @angles[-n].to_f }]
    puts "throttle: #{throttle}"
    tcp.puts tmsg
  end

  def react_to_messages_from_server(tcp)
    previous_message = {}
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      data = message['data']
      case msgType
        when 'carPositions'
          current_bot = data.find {|d| d['id']['name'] == @bot_name }
          play_tick tcp, current_bot
        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
            when 'gameInit'
              puts 'Game init'
              process_track data
            when 'crash'
              puts 'CRASH'
              exit
              puts 'Someone crashed'
              ap previous_message
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{data}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
      previous_message = message
    end
    ap @track_data
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def speed(n=2)
    # distance traveled over last tick
    if @positions.size > 1
       distance_between(@positions.last(n)) / (n - 1)
    else
      @positions.last['inPieceDistance']
    end
  end

  def calculate_speed(piece_position)
  end

  def process_track(track_data)
    @track_data = track_data['race']['track']
    init_pieces @track_data['pieces']
    @lanes = @track_data['lanes']
  end

  ################################ 9.32 ####################### bad ####################
  def naive_throttle          # def naive_throttle         #  def naive_throttle
    case @current_angle.abs   #   case @current_angle.abs  #    case @current_angle.abs
    when 0..0.2               #   when 0..0.2              #    when 0..0.2
      case @current_speed**2  #     case @current_speed**2 #      case @current_speed**2
      when 0..70 then 1       #     when 0..70 then 1      #      when 0..70 then 1
      else 0.1                #     else 0.1               #      else 0.1
      end                     #     end                    #      end
    when 0.2..30              #   when 0.2..30             #    when 0.2..30
      case @current_speed**2  #     case @current_speed**2 #      case @current_speed**2
      when 0..30              #     when 0..30             #      when 0..30
        1                     #       1                    #        1
      when 30..50             #     when 30..50            #      when 30..50
        0.6                   #       0.6                  #        0.6
      else 0.2                #     else 0                 #      else 0.3
      end                     #     end                    #      end
    else                      #   else                     #    else
      0                       #     0                      #      0
    end                       #   end                      #    end
  end                         #                            #  end

  def throttle_for(bot)
    desired_speed_at_nc = speed_for bot['piecePosition']
    0.6
  end

  def init_pieces(pieces)
    @pieces = pieces
    @track_length = pieces.map do |piece|
      ap piece
      curve_length(piece)
    end.reduce :+
    @grip = 0.435
  end

  def is_corner?(piece); !piece['angle'].nil?; end

  def distance_to_next_corner(piece_position)
    nc = next_corner(piece_position['pieceIndex'])
    distance_before(@pieces.find_index { |p| p == nc }) - distance(piece_position)
  end

  def speed_for(piece_position)
    nc = next_corner(piece_position['pieceIndex'])
    speed_for_corner nc
  end

  def next_corner(index)
    @pieces[index..-1].find { |p| is_corner? p } || @pieces.find { |p| is_corner? p }
  end

  def speed_for_corner(piece)
    Math.sqrt @grip * piece['radius']
  end

  def distance_between(pieces)
    distance(pieces.last) - distance(pieces.first)
  end

  def distance(piece_position)
    distance_before = @pieces[0,piece_position['pieceIndex']].map { |pc|
      curve_length(pc)
    }.reduce(0.0, &:+)
    a = ([@track_length * piece_position['lap'],
    distance_before,
    piece_position['inPieceDistance']])
    a.reduce(0.0, &:+)
  end

  def curve_length(piece, offset=0)
    return piece['length'] if piece['length']
    r = piece['radius']
    a = piece['angle'].abs
    a * (Math::PI / 180.0) * r.to_f
  end
end


NoobBot.new(server_host, server_port, bot_name, bot_key)
